//
//  ViewController.swift
//  MobileBankId
//
//  Created by Lachezar Yankov on 04/21/2016.
//  Copyright (c) 2016 Lachezar Yankov. All rights reserved.
//

import UIKit
import MobileBankId

class ViewController: UIViewController {

    var mobileBankIdProvider = MobileBankIdProvider(
        apiKey: "",
        serviceKey: "",
        appScheme: "mybankidscheme"
    )

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate: MobileBankIdAppDelegate = UIApplication.sharedApplication().delegate as! MobileBankIdAppDelegate
        appDelegate.currentMobileBankIdProvider = mobileBankIdProvider
    }

    @IBOutlet weak var bankIdDetails: UITextView!

    @IBAction func startMobileBankIdAuthButtonPressed(sender: AnyObject) {
        mobileBankIdProvider.onAuthSucceeded = {
            [weak self] payload in
            self?.bankIdDetails.text = payload.description
        }
        mobileBankIdProvider.onAuthFailed = {
            [weak self] error, payload in
            if let payload = payload {
                if let code = payload["errorObject"]["code"].string {
                    self?.bankIdDetails.text = "Error details -> \(code)"
                } else {
                    self?.bankIdDetails.text = payload.description
                }
            } else {
                self?.bankIdDetails.text = error.description
            }
        }
        mobileBankIdProvider.authenticate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

