# MobileBankId

## Example project

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

The pod depends on Alamofire 3 and SwiftyJSON. You also need to have a properly setup [BankID app](https://itunes.apple.com/se/app/bankid-sakerhetsapp/id433151512?mt=8).

## Installation

MobileBankId is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MobileBankId"
```

## Usage

The typical usage of the MobileBankId library will be inside a ViewController.

```swift
class ViewController: UIViewController {
    // apiKey and serviceKey are provided from e-identitet.se
    var mobileBankIdProvider = MobileBankIdProvider(apiKey: "...", serviceKey: "...", appScheme: "mybankidscheme")

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate: MobileBankIdAppDelegate = UIApplication.sharedApplication().delegate as! MobileBankIdAppDelegate
        appDelegate.currentMobileBankIdProvider = mobileBankIdProvider
    }

    @IBOutlet weak var bankIdDetails: UITextView!

    @IBAction func startMobileBankIdAuthButtonPressed(sender: AnyObject) {
        mobileBankIdProvider.onAuthSucceeded = {
            [weak self] payload in
            self?.bankIdDetails.text = payload.description
        }
        mobileBankIdProvider.onAuthFailed = {
            [weak self] error, payload in
            if let payload = payload {
                if let code = payload["errorObject"]["code"].string {
                    self?.bankIdDetails.text = "Error details -> \(code)"
                } else {
                    self?.bankIdDetails.text = payload.description
                }
            } else {
                self?.bankIdDetails.text = error.description
            }
        }
        mobileBankIdProvider.authenticate()
    }

    ...
```

The `authenticate()` method will launch the Mobile BankID app and once the authentication process completes, the BankID app will return the control to your app and `application:app:openUrl:options` will be called. You need to make your AppDelegate conform to `MobileBankIdAppDelegate` and add the following code that will complete the authentication process and will fetch user's data.

```swift
class AppDelegate: UIResponder, UIApplicationDelegate, MobileBankIdAppDelegate {
    // currentMobileBankIdProvider must be weak reference, so that it wont let the ViewController leak
    weak var currentMobileBankIdProvider: MobileBankIdProvider?
    ...
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        return currentMobileBankIdProvider?.handleLaunchFromMobileBankIdApp(url: url, options: options) ?? false
    }
}
```

The last step is to register a scheme in info.plist, which your app expects to be used when it is called back from the Mobile BankID app.

```xml
<key>CFBundleURLTypes</key>
<array>
    <dict>
        <key>CFBundleURLName</key>
        <string>com.example.mybankidscheme</string>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>mybankidscheme</string>
        </array>
    </dict>
</array>
```

## Having keys in your app

It is not secured to have secret information (like keys) in your app, because an attacker could manage to extract them, so use it on your own risk. A better approach is to create your own proxy to the GrandID API, which you control fully and limit possible abuse. Here is a little bit more information - [http://stackoverflow.com/questions/14778429/secure-keys-in-ios-app-scenario-is-it-safe](http://stackoverflow.com/questions/14778429/secure-keys-in-ios-app-scenario-is-it-safe).
## Author

Lachezar Yankov, lachezar.yankov@finatech.se

## License

MobileBankId is available under the MIT license. See the LICENSE file for more info.