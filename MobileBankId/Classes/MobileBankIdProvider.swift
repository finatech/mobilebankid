//
//  MobileBankIdProvider.swift
//  Pods
//
//  Created by Lachezar Yankov on 4/21/16.
//
//

import Foundation
import Alamofire
import SwiftyJSON

public class MobileBankIdProvider {

    let appScheme: String

    let baseUrl = "https://client.grandid.com/json1.1/"
    let apiKey: String
    let serviceKey: String

    var sessionId: String?
    var redirectUrl: String?

    public var onAuthSucceeded: (payload: JSON) -> () = { _ in }
    public var onAuthFailed: (error: NSError, payload: JSON?) -> () = { _,_ in }

    public init(apiKey: String, serviceKey: String, appScheme: String) {
        self.appScheme = appScheme
        self.apiKey = apiKey
        self.serviceKey = serviceKey
    }

    public func authenticate() {

        self.sessionId = nil
        self.redirectUrl = nil

        let federatedLoginUrl = "\(baseUrl)FederatedLogin?apiKey=\(apiKey)&authenticateServiceKey=\(serviceKey)"
        let appScheme = self.appScheme

        Alamofire.request(.GET, federatedLoginUrl).responseJSON {
            [weak self] response in

            MobileBankIdProvider.p("FederatedLogin reponse: \(response.result.value)")
            if let value = response.result.value {
                let jsonResponse = JSON(value)
                self?.sessionId = jsonResponse["sessionId"].string
                self?.redirectUrl = jsonResponse["redirectUrl"].string

                if let redirectUrl = self?.redirectUrl {
                    Alamofire.request(.GET, redirectUrl).responseString {
                        response in

                        MobileBankIdProvider.p("redirectUrl response: \(response.result.value)")

                        if let value = response.result.value {
                            if let autostartToken = MobileBankIdProvider.extractAutostartToken(value) {
                                MobileBankIdProvider.p("autostartToken: \(autostartToken)")
                                UIApplication.sharedApplication().openURL(NSURL(string: "bankid:///?autostarttoken=\(autostartToken)&redirect=\(appScheme)://")!)
                            } else {
                                self?.onAuthFailed(error: NSError(domain: "can not extract autostarttoken", code: 1, userInfo: nil), payload: nil)
                            }
                        } else if let error = response.result.error {
                            self?.onAuthFailed(error: error, payload: nil)
                        }
                    }
                } else {
                    self?.onAuthFailed(error: NSError(domain: "Missing redirectUrl", code: 2, userInfo:  nil), payload: nil)
                }
            } else if let error = response.result.error {
                self?.onAuthFailed(error: error, payload: nil)
            }
        }
    }

    public func fetchSessionData() {
        if let sessionId = sessionId, let redirectUrl = redirectUrl {

            let getSessionUrl = "\(baseUrl)GetSession?apiKey=\(apiKey)&authenticateServiceKey=\(serviceKey)&sessionid=\(sessionId)"

            Alamofire.request(.GET, redirectUrl).response {
                _ in

                Alamofire.request(.GET, getSessionUrl).responseJSON {
                    [weak self] response in

                    MobileBankIdProvider.p("GetSession response: \(response.result.value)")

                    if let value = response.result.value {
                        let json = JSON(value)

                        if json["errorObject"].isExists() {
                            // isExists() is changed to exists() in the master branch of SwiftyJSON,
                            // but it is not in any of the available versions
                            self?.onAuthFailed(error: NSError(domain: "BankId Authentication Failure", code: 3, userInfo: nil), payload: json)
                        } else {
                            self?.onAuthSucceeded(payload: json)
                        }
                    } else if let error = response.result.error {
                        self?.onAuthFailed(error: error, payload: nil)
                    }
                }
            }
        }
    }

    public func handleLaunchFromMobileBankIdApp(url url: NSURL, options: [String: AnyObject]) -> Bool {
        if url.scheme == appScheme && options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String == "com.bankidapp.BankID" {
            fetchSessionData()
            return true
        }

        return false
    }

    private static func extractAutostartToken(text: String) -> String? {
        // the text is actually HTML, which is not a regular language, but it does the trick so far
        do {
            let regex = try NSRegularExpression(pattern: "bankid:///\\?autostarttoken=([-0-9a-f]+)&", options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text, options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.rangeAtIndex(1)) }.first
        } catch let error {
            p("Error in extractAutostartToken: \(error)")
            return nil
        }
    }
    
    private static func p(s: String) {
        #if MOBILE_BANK_ID_DEBUG
            print(s)
        #endif
    }
    
    deinit {
        MobileBankIdProvider.p("+++++++++++++ DEINIT MobileBankIdCommunicator")
    }
}
