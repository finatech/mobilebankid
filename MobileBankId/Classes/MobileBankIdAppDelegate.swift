//
//  MobileBankIdAppDelegate.swift
//  Pods
//
//  Created by Lachezar Yankov on 4/21/16.
//
//

public protocol MobileBankIdAppDelegate: UIApplicationDelegate {
    weak var currentMobileBankIdProvider: MobileBankIdProvider? { get set }
}
