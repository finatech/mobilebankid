#
# Be sure to run `pod lib lint MobileBankId.podspec' to ensure this is a
# valid spec before submitting.
#

Pod::Spec.new do |s|
  s.name             = "MobileBankId"
  s.version          = "0.1.0"
  s.summary          = "Mobile BankId integration through e-identitet/GrandID API"

  s.description      = <<-DESC
  Swedish Mobile BankId integration through e-identitet/GrandID API.
  At the moment only GrandID API's authentication is supported.
                       DESC

  s.homepage         = "https://bitbucket.org/finatech/mobilebankid"
  s.license          = 'MIT'
  s.author           = { "Lachezar Yankov" => "lachezar.yankov@finatech.se" }
  s.source           = { :git => "https://bitbucket.org/finatech/mobilebankid.git", :tag => s.version.to_s }

  s.platform = :ios
  s.ios.deployment_target = '9.0'

  s.source_files = 'MobileBankId/Classes/**/*'
  s.resource_bundles = {
    'MobileBankId' => ['MobileBankId/Assets/*.png']
  }

  s.dependency 'Alamofire', '~> 3.0'
  s.dependency 'SwiftyJSON', '~> 2.3.2'
end
